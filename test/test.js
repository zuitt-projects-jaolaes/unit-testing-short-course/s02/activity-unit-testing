const {registration} = require('../src/util.js');
const {assert, expect} = require('chai');


describe('test_registration', () => {
	
	it('age_not_empty', () => {
		const age = "";
		assert.isNotEmpty(registration(age));
	});

	it('age_not_integer', () => {
		const age = "9";
		assert.strictEqual(typeof (registration(age)), 'number', "Error: Age should be number!");
	});

	it('age_not_undefined', () => {
		const age = undefined;
		assert.isDefined(registration(age));
	});

	it('age_not_null', () => {
		const age = null;
		// assert.isNotNull(registration(age));
		expect(registration(age)).to.not.be.null;
	});

	it('age_not_zero', () => {
		const age = 0;
		assert.operator(registration(age), '>', "Error: Age must be greater than 0");
	});

	it('fullName_not_empty', () => {
		const fullName = "";
		assert.isNotEmpty(registration(fullName));
	});

	it('fullName_not_null', () => {
		const fullName = null;
		assert.isNotNull(registration(fullName));
	});

	it('fullName_not_undefined', () => {
		const fullName = undefined;
		assert.isDefined(registration(fullName));
	});

	it('fullName_is_string', () => {
		const fullName = 1;
		assert.isString(registration(fullName));
	});

	it('fullName_not_zero', () => {
		const fullName = "";
		assert.isEmpty(registration(fullName));
	});
})
