

function registration(age, fullName){

	if(age == ""){
		return "Error: Age is Empty!";
	}
	
	if(typeof age != 'number'){
		return "Error: Age should be number!";
	}

	if(age === undefined) {
		return "Error: Age should be defined!";
	}

	if(age === null) {
		return "Error: Age should not be null!";
	}

	if(age.length === 0) {
		return "Error: Age should not be empty";
	}

	return age;


	if(fullName == ""){
		return "Error: Full Name is Empty!";
	}

	if(fullName === null) {
		return "Error: Full Name should not be null!";
	}

	if(fullName === undefined) {
		return "Error: Full Name should be defined!";
	}

	if(typeof(fullName) !== 'string'){
		return "Error: Full Name should be a string value!";
	}

	if(fullName.length === 0) {
		return "Error: Full Name should not be empty";
	}

	return fullName;
}




module.exports = {
	registration: registration
}